@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">Questions</div>

                <div class="panel-body">
                    
                    <multiple-choice-question></multiple-choice-question>

                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <graph :auth_user="{{ Auth::user() }}"></graph>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('users/{id}/scores', function () {

  $options = array(
    'cluster' => 'ap2',
    'encrypted' => true
  );
  $pusher = new Pusher\Pusher(
    '2afd5144b75b33526bfa',
    '7bf7c7972d385e1bef34',
    '404423',
    $options
  );

  $data['scores'] = [7, 8];
  $pusher->trigger('my-channel', 'newScore', $data);

  return 'Done';  

});
